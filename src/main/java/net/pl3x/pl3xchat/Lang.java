package net.pl3x.pl3xchat;

import net.pl3x.pl3xlibs.configuration.BaseConfig;
import Pl3xChat.MyPlugin;

public enum Lang {
	ERROR_NO_PERM("error-no-permission", "&4You do not have permission for that command!"),
	ERROR_NO_CHAT_PERMS("error-no-chat-perms", "&4You do not have permission to chat"),
	ERROR_CHAT_BLANK_MSG("error-chat-blank-msg", "&4Cannot send blank chat message!"),
	ERROR_PLAYER_NOT_FOUND("error-player-not-found", "&4Player not found!"),
	ERROR_UNKNOWN_MISSING_SUBCOMMAND("error-unknown-missing-subcommand", "&4Unknown or missing sub-command!"),
	ERROR_INVALID_COMMAND_STRUCTURE("error-invalid-command-structure", "&4Invalid command structure!"),
	RELOAD("reload", "&dPl3xChat reloaded."),

	BROADCAST_HELP_DESC("broadcast-help-desc", "Broadcast a message to the server"),
	CHANNEL_HELP_DESC("channel-help-desc", "Switch channels or send message to a channel"),
	CLEARCHAT_HELP_DESC("clearchat-help-desc", "Clears your chat"),
	CLEARCHAT_HELP_DESC_ALL("clearchat-help-desc-all", "Clears your chat or all players chat"),
	DEAFEN_HELP_DESC("deafen-help-desc", "Deafen a player from seeing any chat"),
	IGNORE_HELP_DESC("ignore-help-desc", "Ignore a player"),
	MSG_HELP_DESC("msg-help-desc", "Send a message to a player"),
	MUTE_HELP_DESC("mute-help-desc", "Mute a player from speaking in chat"),
	MUTEALL_HELP_DESC("muteall-help-desc", "Temporarily mute all players from speaking in chat"),
	PL3XCHAT_HELP_DESC("pl3xcommands-help-desc", "View version or reload plugin"),
	REPLY_HELP_DESC("reply-help-desc", "Reply to the last message"),
	SOCIALSPY_HELP_DESC("socialspy-help-desc", "Toggle chat spy mode"),
	SOCIALSPY_HELP_DESC_OTHER("socialspy-help-desc-other", "Toggle chat spy mode for yourself or others"),

	BROADCAST_NO_MESSAGE("broadcast-no-message", "&4You must specify a message to broadcast!"),
	BROADCAST_MESSAGE("broadcast-message", "&e[&dbroadcast&e] &6{message}"),
	CHANNEL_NOT_FOUND("channel-not-found", "&4Channel not found!"),
	CHANNEL_ALREADY_CURRENT("channel-already-current", "&4You are already in that channel!"),
	CHANNEL_CANNOT_JOIN("channel-cannot-join", "&4You cannot join that channel!"),
	CHANNEL_SET("channel-set", "&dYou are now in channel &7{channel}"),
	DEAFEN_TARGET_EXEMPT("deafen-target-exempt", "&4You cannot deafen that player!"),
	DEAFEN_TARGET_MSG("deafen-target-msg", "&dYou are now &7{action}&d."),
	DEAFEN_PLAYER_MSG("deafen-player-msg", "&7{target}&d is now &7{action}&d."),
	IGNORE_PLAYER_MSG("ignore-player-msg", "&7{target} &dis now &7{action}&d."),
	IGNORE_YOU_ARE_IGNORED("ignore-you-are-ignored", "&7{target} &4has you ignored!"),
	IGNORE_TARGET_EXEMPT("ignore-target-exempt", "&4You cannot ignore that player!"),
	MSG_BLANK_MSG("msg-blank-msg", "&4You cannot send a blank message!"),
	MSG_PLAYER("msg-player", "&e[&7Me &d-> &7{target}&e] &7{message}"),
	MSG_TARGET("msg-target", "&e[&7{player} &d-> &7Me&e] &7{message}"),
	MSG_OTHER("msg-other", "&e[&7{player} &d-> &7{target}&e] &7{message}"),
	MUTE_TARGET_EXEMPT("mute-target-exempt", "&4You cannot mute that player!"),
	MUTE_TARGET_MSG("mute-target-msg", "&dYou have been &7{action}&d."),
	MUTE_PLAYER_MSG("mute-player-msg", "&7{target}&d is now &7{action}&d."),
	MUTE_YOU_ARE_MUTED("mute-you-are-muted", "&4Cannot send chat. You are muted!"),
	MUTEALL_PLAYER_MSG("muteall-player-msg", "&dYou have &7{action} &dall online players."),
	REPLY_BLANK_MSG("reply-blank-msg", "&4You cannot send a blank message!"),
	REPLY_NO_TARGET("reply-no-target", "&4You have nothing to reply to!"),
	REPLY_PLAYER("reply-player", "&e[&7Me &d-> &7{target}&e] &7{message}"),
	REPLY_TARGET("reply-target", "&e[&7{player} &d-> &7Me&e] &7{message}"),
	REPLY_OTHER("reply-other", "&e[&7{player} &d-> &7{target}&e] &7{message}"),
	SOCIALSPY_NO_SPY_CHANNEL("socialspy-no-spy-channel", "&4Cannot enter spy mode! There is no Spy channel configured!"),
	SOCIALSPY_TARGET_EXEMPT("socialspy-target-exempt", "&4You cannot toggle that player's social spy mode!"),
	SOCIALSPY_TOGGLED("socialspy-toggled", "&dSocial spy mode &7{toggle}&d."),
	SOCIALSPY_TOGGLED_OTHER("socialspy-toggled-other", "&dSocial spy mode &7{toggle}&d for &7{target}&d.");

	private String key;
	private String def;

	private static BaseConfig config;

	private Lang(String key, String def) {
		this.key = key;
		this.def = def;
		init();
	}

	private static void init() {
		config = new BaseConfig(MyPlugin.class, MyPlugin.getInstance().getPluginInfo(), "", MyPlugin.getInstance().getConfig().get("language-file", "lang-en.ini"));
		config.load();
	}

	public static void refreshAll() {
		config = null;
		init();
	}

	public String get() {
		String value = config.get(key, def);
		if (value == null) {
			// This shouldn't happen
			value = "&c[missing lang data]";
		}
		return value;
	}
}
