package net.pl3x.pl3xchat;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class SpyList {
	private Set<UUID> socialSpyMode = new HashSet<UUID>();

	public void addSpy(UUID uuid) {
		socialSpyMode.add(uuid);
	}

	public void removeSpy(UUID uuid) {
		if (!hasSpyMode(uuid)) {
			return;
		}
		socialSpyMode.remove(uuid);
	}

	public boolean hasSpyMode(UUID uuid) {
		return socialSpyMode.contains(uuid);
	}
}
