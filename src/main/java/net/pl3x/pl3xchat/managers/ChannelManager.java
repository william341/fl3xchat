package net.pl3x.pl3xchat.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import net.pl3x.pl3xchat.Channel;
import Pl3xChat.MyPlugin;

public class ChannelManager {
	private MyPlugin plugin;
	private Set<Channel> channels = new HashSet<Channel>();
	private HashMap<UUID, Channel> players = new HashMap<UUID, Channel>();

	public ChannelManager(MyPlugin plugin) {
		this.plugin = plugin;
		loadChannels();
	}

	public Channel getCurrentChannel(UUID sender) {
		if (players.containsKey(sender)) {
			return players.get(sender);
		}
		return getDefaultChannel();
	}

	public void setCurrentChannel(UUID sender, Channel channel) {
		players.put(sender, channel);
	}

	public Channel getDefaultChannel() {
		String def = plugin.getConfig().get("default-channel", "local");
		for (Channel channel : channels) {
			if (channel.getName().equalsIgnoreCase(def)) {
				return channel;
			}
		}
		return null;
	}

	public Channel getChannel(String name) {
		for (Channel channel : channels) {
			if (channel.getName().equalsIgnoreCase(name)) {
				return channel;
			}
		}
		return null;
	}

	public List<String> getMatchingChannels(String arg) {
		List<String> list = new ArrayList<String>();
		for (Channel channel : channels) {
			if (channel.getName().toLowerCase().startsWith(arg.toLowerCase())) {
				list.add(channel.getName());
			}
		}
		return list;
	}

	private void loadChannels() {
		channels.clear();
		List<String> enabledChannels = plugin.getConfig().getStringList("enabled-channels");
		if (enabledChannels == null || enabledChannels.isEmpty()) {
			plugin.getLogger().error("No Channels Enabled!");
			return;
		}
		for (String channelName : enabledChannels) {
			channelName = channelName.toLowerCase();
			String name = plugin.getChannelConfig().get(channelName + "-name");
			Boolean canJoin = plugin.getChannelConfig().getBoolean(channelName + "-can-join");
			Integer radius = plugin.getChannelConfig().getInteger(channelName + "-radius");
			Boolean allWorlds = plugin.getChannelConfig().getBoolean(channelName + "-all-worlds");
			Boolean needPerms = plugin.getChannelConfig().getBoolean(channelName + "-need-perms");
			String format = plugin.getChannelConfig().get(channelName + "-format");
			if (name == null || name.equals("") || canJoin == null || radius == null || allWorlds == null || needPerms == null || format == null || format.equals("")) {
				plugin.getLogger().error("Cannot load channel: " + channelName);
				continue;
			}
			Channel channel = new Channel(name);
			channel.setCanJoin(canJoin);
			channel.setRadius(radius);
			channel.setAllWorlds(allWorlds);
			channel.setFormat(format);
			channel.setNeedPerms(needPerms);
			channels.add(channel);
		}
	}
}
