package net.pl3x.pl3xchat.managers;

import java.util.UUID;

import net.pl3x.pl3xchat.Channel;
import net.pl3x.pl3xchat.DeafList;
import net.pl3x.pl3xchat.IgnoreList;
import net.pl3x.pl3xchat.Lang;
import net.pl3x.pl3xchat.Message;
import net.pl3x.pl3xchat.MuteList;
import net.pl3x.pl3xchat.SpyList;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xChat.MyPlugin;
import PluginReference.MC_Player;

public class ChatManager {
	private MyPlugin plugin;
	private ChannelManager channelManager;
	private IgnoreList ignoreList;
	private DeafList deafList;
	private MuteList muteList;
	private SpyList spyList;

	public ChatManager(MyPlugin plugin) {
		this.plugin = plugin;
		channelManager = new ChannelManager(plugin);
		ignoreList = new IgnoreList();
		deafList = new DeafList();
		muteList = new MuteList();
		spyList = new SpyList();
	}

	public void sendChat(MC_Player sender, String msg) {
		if (getMuteList().isPlayerMuted(sender.getUUID())) {
			Pl3xLibs.sendMessage(sender, Lang.MUTE_YOU_ARE_MUTED.get());
			return;
		}
		if (!sender.hasPermission("pl3xchat.allow.color")) {
			msg = Pl3xLibs.stripColors(msg);
		}
		if (!sender.hasPermission("pl3xchat.allow.style")) {
			msg = Pl3xLibs.stripStyles(msg);
		}
		Channel channel = getChannelManager().getCurrentChannel(sender.getUUID());
		Message message = new Message(sender.getUUID(), msg);
		message = calculateRecipients(message, channel);
		for (UUID recipient : message.getRecipients()) {
			if (getIgnoreList().isIgnoring(recipient, message.getSender())) {
				continue; // recipient is ignoring sender
			}
			if (getDeafList().isPlayerDeaf(recipient) && !recipient.equals(message.getSender())) {
				continue; // recipient is deaf
			}
			if (channel.needPerms() && !Pl3xLibs.getPlayer(recipient).hasPermission("pl3xchat.channel." + channel.getName().toLowerCase())) {
				continue; // has no needed perm to hear channel
			}
			Pl3xLibs.sendMessage(recipient, message.compile(channel.getFormat()));
		}
		for (UUID recipient : message.getSpies()) {
			// spies bypass ignore and deaf lists
			Channel spyChannel = getChannelManager().getChannel("spy");
			if (spyChannel == null) {
				return; // spy channel is not configured!
			}
			Pl3xLibs.sendMessage(recipient, message.compile(spyChannel.getFormat()));
		}
	}

	public Message calculateRecipients(Message message, Channel channel) {
		MC_Player sender = Pl3xLibs.getPlayer(message.getSender());
		int radius = channel.getRadius();
		// calculate recipients
		message.addRecipient(message.getSender()); // always add self
		if (radius <= 0) {
			if (channel.allWorlds()) { // Universe
				for (MC_Player recipient : plugin.getServer().getPlayers()) {
					message.addRecipient(recipient.getUUID());
				}
			} else { // Global
				String worldName = sender.getWorld().getName();
				for (MC_Player recipient : plugin.getServer().getPlayers()) {
					if (recipient.getWorld().getName().equals(worldName)) {
						message.addRecipient(recipient.getUUID());
					}
				}
			}
		} else { // Local with radius
			for (MC_Player recipient : Pl3xLibs.getNearbyPlayers(sender, radius)) {
				message.addRecipient(recipient.getUUID());
			}
		}
		// calculate spies
		for (MC_Player spy : plugin.getServer().getPlayers()) {
			if (message.getRecipients().contains(spy.getUUID())) {
				continue; // already a recipient
			}
			if (!getSpyList().hasSpyMode(spy.getUUID())) {
				continue; // not a spy
			}
		}
		return message;
	}

	public ChannelManager getChannelManager() {
		return channelManager;
	}

	public IgnoreList getIgnoreList() {
		return ignoreList;
	}

	public MuteList getMuteList() {
		return muteList;
	}

	public DeafList getDeafList() {
		return deafList;
	}

	public SpyList getSpyList() {
		return spyList;
	}
}
