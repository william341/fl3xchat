package net.pl3x.pl3xchat;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class MuteList {
	private Set<UUID> muted = new HashSet<UUID>();

	public void mutePlayer(UUID uuid) {
		muted.add(uuid);
	}

	public void unmutePlayer(UUID uuid) {
		if (!isPlayerMuted(uuid)) {
			return;
		}
		muted.remove(uuid);
	}

	public boolean isPlayerMuted(UUID uuid) {
		return muted.contains(uuid);
	}
}
