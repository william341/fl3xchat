package net.pl3x.pl3xchat.commands;

import java.util.List;

import net.pl3x.pl3xchat.Alias;
import net.pl3x.pl3xchat.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xChat.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Pl3xChat implements MC_Command {
	private MyPlugin plugin;

	public Pl3xChat(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.PL3XCHAT.get();
	}

	@Override
	public String getCommandName() {
		return "pl3xchat";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/pl3xchat (reload) - " + Lang.PL3XCHAT_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7pl3xchat &e(&7reload&e) &a- &d" + Lang.PL3XCHAT_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
			plugin.reload();
			Pl3xLibs.sendMessage(player, Lang.RELOAD.get());
			return;
		}
		Pl3xLibs.sendMessage(player, "&d" + plugin.getPluginInfo().name + " v&7" + plugin.getPluginInfo().version + "&d.");
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.pl3xchat");
	}
}
