package net.pl3x.pl3xchat.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.pl3x.pl3xchat.Alias;
import net.pl3x.pl3xchat.Lang;
import net.pl3x.pl3xchat.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xChat.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Ignore implements MC_Command {
	private MyPlugin plugin;

	public Ignore(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.IGNORE.get();
	}

	@Override
	public String getCommandName() {
		return "ignore";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7ignore &e<&7player&e> &a- &d" + Lang.IGNORE_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length < 1) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		MC_Player target = Pl3xLibs.getOfflinePlayer(args[0]);
		if (target == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		if (target.hasPermission("command.ignore.exempt")) {
			Pl3xLibs.sendMessage(player, Lang.IGNORE_TARGET_EXEMPT.get());
			return;
		}
		List<UUID> ignoreList = plugin.getChatManager().getIgnoreList().getPlayerIgnoreList(player.getUUID());
		boolean ignoring = true;
		if (ignoreList.contains(target.getUUID())) {
			ignoreList.remove(target.getUUID());
			ignoring = false;
		} else {
			ignoreList.add(target.getUUID());
		}
		plugin.getChatManager().getIgnoreList().setIgnoreList(player.getUUID(), ignoreList);
		List<String> ignoreStringList = new ArrayList<String>();
		for (UUID ignored : ignoreList) {
			ignoreStringList.add(ignored.toString());
		}
		PlayerConfig.getConfig(plugin, player.getUUID()).set("ignoring", Pl3xLibs.join(ignoreStringList, ",", 0));
		Pl3xLibs.sendMessage(player, Lang.IGNORE_PLAYER_MSG.get().replace("{target}", target.getCustomName()).replace("{action}", ignoring ? "ignored" : "not ignored"));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.ignore");
	}
}
