package net.pl3x.pl3xchat.commands;

import java.util.List;

import net.pl3x.pl3xchat.Alias;
import net.pl3x.pl3xchat.Lang;
import net.pl3x.pl3xchat.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xChat.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Deafen implements MC_Command {
	private MyPlugin plugin;

	public Deafen(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.DEAFEN.get();
	}

	@Override
	public String getCommandName() {
		return "deafen";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/deafen <player> - " + Lang.DEAFEN_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7deafen &e<&7player&e> &a- &d" + Lang.DEAFEN_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_UNKNOWN_MISSING_SUBCOMMAND.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		MC_Player target = Pl3xLibs.getOfflinePlayer(args[0]);
		if (target == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		if (target.hasPermission("command.deafen.exempt")) {
			Pl3xLibs.sendMessage(player, Lang.DEAFEN_TARGET_EXEMPT.get());
			return;
		}
		PlayerConfig config = PlayerConfig.getConfig(plugin, target.getUUID());
		boolean deaf = !config.getBoolean("deaf");
		config.set("deaf", deaf ? "true" : "false");
		if (deaf) {
			plugin.getChatManager().getDeafList().deafenPlayer(target.getUUID());
		} else {
			plugin.getChatManager().getDeafList().undeafenPlayer(target.getUUID());
		}
		MC_Player online = Pl3xLibs.getPlayer(target.getCustomName());
		if (online != null) {
			Pl3xLibs.sendMessage(online, Lang.DEAFEN_TARGET_MSG.get().replace("{action}", deaf ? "deaf" : "undeaf"));
		}
		Pl3xLibs.sendMessage(player, Lang.DEAFEN_PLAYER_MSG.get().replace("{action}", deaf ? "deaf" : "undeaf").replace("{target}", target.getCustomName()));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.deafen");
	}
}
