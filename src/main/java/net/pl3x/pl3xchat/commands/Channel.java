package net.pl3x.pl3xchat.commands;

import java.util.List;

import net.pl3x.pl3xchat.Alias;
import net.pl3x.pl3xchat.Lang;
import net.pl3x.pl3xchat.managers.ChannelManager;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xChat.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Channel implements MC_Command {
	private MyPlugin plugin;

	public Channel(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.CHANNEL.get();
	}

	@Override
	public String getCommandName() {
		return "channel";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7channel &e<&7channel&e> &e(&7message&e) &a- &d" + Lang.CHANNEL_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return plugin.getChatManager().getChannelManager().getMatchingChannels(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_UNKNOWN_MISSING_SUBCOMMAND.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		ChannelManager channelManager = plugin.getChatManager().getChannelManager();
		net.pl3x.pl3xchat.Channel channel = channelManager.getChannel(args[0]);
		if (channel == null) {
			Pl3xLibs.sendMessage(player, Lang.CHANNEL_NOT_FOUND.get());
			return;
		}
		if (channelManager.getCurrentChannel(player.getUUID()).getName().equalsIgnoreCase(channel.getName())) {
			Pl3xLibs.sendMessage(player, Lang.CHANNEL_ALREADY_CURRENT.get());
			return;
		}
		if (!channel.canJoin()) {
			Pl3xLibs.sendMessage(player, Lang.CHANNEL_CANNOT_JOIN.get());
			return;
		}
		if (channel.needPerms() && !player.hasPermission("pl3xchat.channel." + channel.getName().toLowerCase())) {
			Pl3xLibs.sendMessage(player, Lang.CHANNEL_CANNOT_JOIN.get());
			return;
		}
		channelManager.setCurrentChannel(player.getUUID(), channel);

		Pl3xLibs.sendMessage(player, Lang.CHANNEL_SET.get().replace("{channel}", channel.getName()));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.channel");
	}
}
