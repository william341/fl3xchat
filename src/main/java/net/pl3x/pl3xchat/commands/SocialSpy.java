package net.pl3x.pl3xchat.commands;

import java.util.List;
import java.util.UUID;

import net.pl3x.pl3xchat.Alias;
import net.pl3x.pl3xchat.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xChat.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class SocialSpy implements MC_Command {
	private MyPlugin plugin;

	public SocialSpy(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.SOCIALSPY.get();
	}

	@Override
	public String getCommandName() {
		return "socialspy";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/socialspy (player) - " + Lang.SOCIALSPY_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.socialspy.other")) {
			return Pl3xLibs.colorize("&e/&7socialspy &e(&7player&e) &a- &d" + Lang.SOCIALSPY_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7socialspy &a- &d" + Lang.SOCIALSPY_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (plugin.getChatManager().getChannelManager().getChannel("spy") == null) {
			Pl3xLibs.sendMessage(player, Lang.SOCIALSPY_NO_SPY_CHANNEL.get());
			return;
		}
		if (args.length > 0) {
			if (player != null && !player.hasPermission("command.socialspy.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			MC_Player target = Pl3xLibs.getPlayer(args[0]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.socialspy.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.SOCIALSPY_TARGET_EXEMPT.get());
				return;
			}
			UUID uuid = target.getUUID();
			boolean enable = false;
			if (plugin.getChatManager().getSpyList().hasSpyMode(uuid)) {
				plugin.getChatManager().getSpyList().removeSpy(uuid);
			} else {
				plugin.getChatManager().getSpyList().addSpy(uuid);
				enable = true;
			}
			Pl3xLibs.sendMessage(target, Lang.SOCIALSPY_TOGGLED.get().replace("{toggle}", enable ? "enabled" : "disabled"));
			Pl3xLibs.sendMessage(player, Lang.SOCIALSPY_TOGGLED_OTHER.get().replace("{target}", target.getCustomName()).replace("{toggle}", enable ? "enabled" : "disabled"));
			return;
		}
		UUID uuid = player.getUUID();
		boolean enable = false;
		if (plugin.getChatManager().getSpyList().hasSpyMode(uuid)) {
			plugin.getChatManager().getSpyList().removeSpy(uuid);
		} else {
			plugin.getChatManager().getSpyList().addSpy(uuid);
			enable = true;
		}
		Pl3xLibs.sendMessage(player, Lang.SOCIALSPY_TOGGLED.get().replace("{toggle}", enable ? "enabled" : "disabled"));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.socialspy");
	}
}
