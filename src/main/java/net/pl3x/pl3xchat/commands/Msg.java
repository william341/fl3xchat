package net.pl3x.pl3xchat.commands;

import java.util.List;

import net.pl3x.pl3xchat.Alias;
import net.pl3x.pl3xchat.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xChat.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Msg implements MC_Command {
	private MyPlugin plugin;

	public Msg(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.MSG.get();
	}

	@Override
	public String getCommandName() {
		return "msg";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7msg &e<&7player&e> <&7message&e> &a- &d" + Lang.MSG_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length < 2) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String message = Pl3xLibs.join(args, " ", 1);
		if (message == null || message.equals("")) {
			Pl3xLibs.sendMessage(player, Lang.MSG_BLANK_MSG.get());
			return;
		}
		MC_Player target = Pl3xLibs.getPlayer(args[0]);
		if (target == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		if (plugin.getChatManager().getIgnoreList().isIgnoring(target.getUUID(), player.getUUID())) {
			Pl3xLibs.sendMessage(player, Lang.IGNORE_YOU_ARE_IGNORED.get().replace("{target}", target.getCustomName()));
			return;
		}
		synchronized (Reply.replydb) {
			Reply.replydb.put(player.getUUID(), target.getUUID());
			Reply.replydb.put(target.getUUID(), player.getUUID());
		}
		Pl3xLibs.sendMessage(target, Lang.MSG_TARGET.get().replace("{player}", player.getCustomName()).replace("{target}", target.getCustomName()).replace("{message}", message));
		Pl3xLibs.sendMessage(player, Lang.MSG_PLAYER.get().replace("{player}", player.getCustomName()).replace("{target}", target.getCustomName()).replace("{message}", message));
		for (MC_Player spy : plugin.getServer().getPlayers()) {
			if (spy.getCustomName().equals(target.getCustomName())) {
				continue; // target doesnt need to spy
			}
			if (spy.getCustomName().equals(player.getCustomName())) {
				continue; // sender doesnt need to spy
			}
			if (!plugin.getChatManager().getSpyList().hasSpyMode(spy.getUUID())) {
				continue; // not in spy mode
			}
			Pl3xLibs.sendMessage(spy, Lang.MSG_OTHER.get().replace("{player}", player.getCustomName()).replace("{target}", target.getCustomName()).replace("{message}", message));
		}
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.msg");
	}
}
