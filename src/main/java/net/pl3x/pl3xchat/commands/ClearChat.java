package net.pl3x.pl3xchat.commands;

import java.util.List;

import net.pl3x.pl3xchat.Alias;
import net.pl3x.pl3xchat.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xChat.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class ClearChat implements MC_Command {
	private MyPlugin plugin;

	public ClearChat(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.CLEARCHAT.get();
	}

	@Override
	public String getCommandName() {
		return "clearchat";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player.hasPermission("command.clearchat.all")) {
			return Pl3xLibs.colorize("&e/&7clearchat &e(&7all&e) &a- &d" + Lang.CLEARCHAT_HELP_DESC_ALL.get());
		}
		return Pl3xLibs.colorize("&e/&7clearchat &a- &d" + Lang.CLEARCHAT_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length > 0 && args[0].equalsIgnoreCase("all")) {
			if (!player.hasPermission("command.clearchat.all")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			for (MC_Player target : plugin.getServer().getPlayers()) {
				if (target.hasPermission("command.clearchat.exempt")) {
					continue;
				}
				for (int i = 0; i < 150; i++) {
					target.sendMessage(" ");
				}
			}
			return;
		}
		for (int i = 0; i < 150; i++) {
			player.sendMessage(" ");
		}
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.clearchat");
	}
}
