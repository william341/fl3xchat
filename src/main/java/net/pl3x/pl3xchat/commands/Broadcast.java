package net.pl3x.pl3xchat.commands;

import java.util.List;

import net.pl3x.pl3xchat.Alias;
import net.pl3x.pl3xchat.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xChat.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Broadcast implements MC_Command {
	private MyPlugin plugin;

	public Broadcast(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.BROADCAST.get();
	}

	@Override
	public String getCommandName() {
		return "broadcast";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/broadcast - " + Lang.BROADCAST_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7broadcast &e<&7message&e> &a- &d" + Lang.BROADCAST_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.BROADCAST_NO_MESSAGE.get());
		}
		plugin.getServer().broadcastMessage(Pl3xLibs.colorize(Lang.BROADCAST_MESSAGE.get().replace("{message}", Pl3xLibs.join(args, " ", 0))));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.broadcast");
	}
}
