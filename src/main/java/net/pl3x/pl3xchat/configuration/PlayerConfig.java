package net.pl3x.pl3xchat.configuration;

import java.io.File;
import java.util.HashMap;
import java.util.UUID;

import net.pl3x.pl3xlibs.configuration.BaseConfig;
import Pl3xChat.MyPlugin;

public class PlayerConfig extends BaseConfig {
	private static final HashMap<UUID, PlayerConfig> configs = new HashMap<UUID, PlayerConfig>();

	public static PlayerConfig getConfig(MyPlugin plugin, UUID uuid) {
		synchronized (configs) {
			if (configs.containsKey(uuid)) {
				return configs.get(uuid);
			}
			PlayerConfig config = new PlayerConfig(plugin, uuid);
			configs.put(uuid, config);
			return config;
		}
	}

	public static void removeConfig(UUID uuid) {
		synchronized (configs) {
			if (!configs.containsKey(uuid)) {
				return;
			}
			configs.remove(uuid);
		}
	}

	public static void removeAllConfigs() {
		synchronized (configs) {
			configs.clear();
		}
	}

	public static void saveAllConfigs() {
		synchronized (configs) {
			for (PlayerConfig config : configs.values()) {
				config.save();
			}
		}
	}

	public PlayerConfig(MyPlugin plugin, UUID uuid) {
		super(MyPlugin.class, plugin.getPluginInfo(), "userdata" + File.separator, uuid.toString() + ".ini");
		load(false);
	}
}
