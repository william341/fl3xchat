package net.pl3x.pl3xchat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class IgnoreList {
	private HashMap<UUID, List<UUID>> ignore = new HashMap<UUID, List<UUID>>();

	public List<UUID> getPlayerIgnoreList(UUID uuid) {
		if (!ignore.containsKey(uuid))
			return new ArrayList<UUID>();
		return ignore.get(uuid);
	}

	public void setIgnoreList(UUID uuid, List<UUID> list) {
		this.ignore.put(uuid, list);
	}

	public boolean isIgnoring(UUID ignorer, UUID ignored) {
		return getPlayerIgnoreList(ignorer).contains(ignored);
	}
}
