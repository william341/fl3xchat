package net.pl3x.pl3xchat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xChat.MyPlugin;
import PluginReference.MC_Player;

public class Message {
	private UUID sender;
	private String message;
	private Set<UUID> recipients = new HashSet<UUID>();
	private Set<UUID> spies = new HashSet<UUID>();

	public Message(UUID sender, String message) {
		this.sender = sender;
		this.message = message;
	}

	public UUID getSender() {
		return sender;
	}

	public String getMessage() {
		return message;
	}

	public Set<UUID> getRecipients() {
		return recipients;
	}

	public void setRecipients(Set<UUID> recipients) {
		this.recipients = recipients;
	}

	public void addRecipient(UUID recipient) {
		recipients.add(recipient);
	}

	public Set<UUID> getSpies() {
		return spies;
	}

	public void setSpies(Set<UUID> spies) {
		this.spies = spies;
	}

	public String compile(String format) {
		MC_Player player = Pl3xLibs.getPlayer(sender);
		String msg = format.replace("{timestamp}", getTimeStamp())
				.replace("{world}", Pl3xLibs.getWorldName(player.getWorld()))
				.replace("{prefix}", getPrefix(player))
				.replace("{username}", player.getCustomName())
				.replace("{suffix}", getSuffix(player))
				.replace("{message}", message);
		return msg;
	}

	private String getTimeStamp() {
		String format = MyPlugin.getInstance().getConfig().get("timestamp-format", "hh:mm a");
		return new SimpleDateFormat(format).format(new Date());
	}

	private String getPrefix(MC_Player player) {
		if (player.isOp()) {
			return "";
		} else {
			List<String> prefixes = MyPlugin.getInstance().getConfig().getStringList("enabled-prefixes");
			for (String prefix : prefixes) {
				if (player.hasPermission("pl3xchat.prefix." + prefix)) {
					return MyPlugin.getInstance().getConfig().get("prefix-" + prefix);
				}
			}
		}
		return "";
	}

	private String getSuffix(MC_Player player) {
		if (player.isOp()) {
			return "";
		} else {
			List<String> suffixes = MyPlugin.getInstance().getConfig().getStringList("enabled-suffixes");
			for (String suffix : suffixes) {
				if (player.hasPermission("pl3xchat.suffix." + suffix)) {
					return MyPlugin.getInstance().getConfig().get("suffix-" + suffix);
				}
			}
		}
		return "";
	}
}
