package net.pl3x.pl3xchat;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class DeafList {
	private Set<UUID> deaf = new HashSet<UUID>();

	public void deafenPlayer(UUID uuid) {
		deaf.add(uuid);
	}

	public void undeafenPlayer(UUID uuid) {
		if (!isPlayerDeaf(uuid)) {
			return;
		}
		deaf.remove(uuid);
	}

	public boolean isPlayerDeaf(UUID uuid) {
		return deaf.contains(uuid);
	}
}
