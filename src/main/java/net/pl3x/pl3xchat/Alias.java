package net.pl3x.pl3xchat;

import java.util.Arrays;
import java.util.List;

import net.pl3x.pl3xlibs.configuration.BaseConfig;
import Pl3xChat.MyPlugin;

public enum Alias {
	BROADCAST("broadcast", "bc,broadc,bcast"),
	CHANNEL("channel", "ch"),
	CLEARCHAT("clearchat", "cc"),
	DEAFEN("deafen", "deaf"),
	IGNORE("ignore", "forget"),
	MSG("msg", "message,send,tell,whisper,w,pm"),
	MUTE("mute", "silence"),
	MUTEALL("muteall", "silenceall"),
	PL3XCHAT("pl3xchat", ""),
	REPLY("reply", "r"),
	SOCIALSPY("socialspy", "chatspy");

	private String key;
	private String def;

	private static BaseConfig config;

	private Alias(String key, String def) {
		this.key = key;
		this.def = def;
		init();
	}

	private static void init() {
		config = new BaseConfig(MyPlugin.class, MyPlugin.getInstance().getPluginInfo(), "", "aliases.ini");
		config.load();
	}

	public static void refreshAll() {
		config = null;
		init();
	}

	public List<String> get() {
		String value = config.get(key, def).replace(" ", "");
		if (value == null || value.equals("")) {
			// This shouldn't happen
			return Arrays.asList(new String[] {});
		}
		return Arrays.asList(value.split(","));
	}
}
