package net.pl3x.pl3xchat;

public class Channel {
	private String name;
	private boolean canJoin = true;
	private int radius = 0;
	private boolean allWorlds = true;
	private boolean needPerms = false;
	private String format;

	public Channel(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public boolean canJoin() {
		return canJoin;
	}

	public void setCanJoin(boolean canJoin) {
		this.canJoin = canJoin;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public boolean allWorlds() {
		return allWorlds;
	}

	public void setAllWorlds(boolean allWorlds) {
		this.allWorlds = allWorlds;
	}

	public boolean needPerms() {
		return needPerms;
	}

	public void setNeedPerms(boolean needPerms) {
		this.needPerms = needPerms;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
}
