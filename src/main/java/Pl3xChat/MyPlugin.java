package Pl3xChat;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.pl3x.pl3xchat.commands.Broadcast;
import net.pl3x.pl3xchat.commands.Channel;
import net.pl3x.pl3xchat.commands.ClearChat;
import net.pl3x.pl3xchat.commands.Deafen;
import net.pl3x.pl3xchat.commands.Ignore;
import net.pl3x.pl3xchat.commands.Msg;
import net.pl3x.pl3xchat.commands.Mute;
import net.pl3x.pl3xchat.commands.MuteAll;
import net.pl3x.pl3xchat.commands.Pl3xChat;
import net.pl3x.pl3xchat.commands.Reply;
import net.pl3x.pl3xchat.commands.SocialSpy;
import net.pl3x.pl3xchat.configuration.PlayerConfig;
import net.pl3x.pl3xchat.managers.ChatManager;
import net.pl3x.pl3xchat.runnables.StartMetrics;
import net.pl3x.pl3xlibs.Logger;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.configuration.BaseConfig;
import PluginReference.MC_EventInfo;
import PluginReference.MC_Player;
import PluginReference.MC_Server;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

public class MyPlugin extends PluginBase {
	private static MyPlugin instance;
	private Logger logger = null;
	private BaseConfig config = null;
	private BaseConfig channelConfig = null;
	private ChatManager chatManager = null;
	private MC_Server server;

	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.name = "Pl3xChat";
		info.version = "0.2-SNAPSHOT";
		info.description = "Powerful chat controls";
		info.optionalData.put("plugin-directory", "plugins_mod" + File.separator + info.name + File.separator);
		return info;
	}

	public static MyPlugin getInstance() {
		return instance;
	}

	@Override
	public void onStartup(MC_Server argServer) {
		server = argServer;
		instance = this;
		init();
		registerCommands();
		getLogger().info(getPluginInfo().name + " v" + getPluginInfo().version + " by BillyGalbreath is now enabled.");
	}

	@Override
	public void onShutdown() {
		disable();
		getLogger().info("Plugin disabled.");
	}

	public Logger getLogger() {
		if (logger == null) {
			logger = Pl3xLibs.getLogger(getPluginInfo());
		}
		return logger;
	}

	public MC_Server getServer() {
		return server;
	}

	public ChatManager getChatManager() {
		return chatManager;
	}

	public BaseConfig getConfig() {
		if (config == null) {
			config = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "config.ini");
			config.load();
		}
		return config;
	}

	public BaseConfig getChannelConfig() {
		if (channelConfig == null) {
			channelConfig = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "channels.ini");
			channelConfig.load();
		}
		return channelConfig;
	}

	private void init() {
		getConfig(); // initialize the config.ini file

		chatManager = new ChatManager(this);

		Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new StartMetrics(this), 100);
	}

	private void disable() {
		Pl3xLibs.getScheduler().cancelAllRunnables(getPluginInfo().name);
		PlayerConfig.saveAllConfigs();
		PlayerConfig.removeAllConfigs();
		config = null;
		channelConfig = null;
		logger = null;
		chatManager = null;
	}

	public void reload() {
		disable();
		init();
	}

	private void registerCommands() {
		getServer().registerCommand(new Broadcast(this));
		getServer().registerCommand(new Channel(this));
		getServer().registerCommand(new ClearChat(this));
		getServer().registerCommand(new Deafen(this));
		getServer().registerCommand(new Ignore(this));
		getServer().registerCommand(new Msg(this));
		getServer().registerCommand(new Mute(this));
		getServer().registerCommand(new MuteAll(this));
		getServer().registerCommand(new Pl3xChat(this));
		getServer().registerCommand(new Reply(this));
		getServer().registerCommand(new SocialSpy(this));
	}

	@Override
	public void onPlayerJoin(MC_Player player) {
		UUID uuid = player.getUUID();
		PlayerConfig config = PlayerConfig.getConfig(this, uuid);
		if (config.getBoolean("muted", false)) {
			getChatManager().getMuteList().mutePlayer(uuid);
		}
		if (config.getBoolean("deaf", false)) {
			getChatManager().getDeafList().deafenPlayer(uuid);
		}
		// retrieve player's ignore list
		List<String> ignoreStringList = config.getStringList("ignoring");
		if (ignoreStringList != null && !ignoreStringList.isEmpty()) {
			List<UUID> ignoreList = new ArrayList<UUID>();
			for (String ignored : ignoreStringList) {
				if (ignored == null || ignored.equals("")) {
					continue; // config contains empty ignore list "ignored=" skip it
				}
				ignoreList.add(UUID.fromString(ignored));
			}
			getChatManager().getIgnoreList().setIgnoreList(player.getUUID(), ignoreList);
		}
	}

	@Override
	public void onPlayerLogout(String playerName, UUID uuid) {
		// remove player from spy mode
		if (getChatManager().getSpyList().hasSpyMode(uuid)) {
			getChatManager().getSpyList().removeSpy(uuid);
		}
	}

	@Override
	public void onPlayerInput(MC_Player player, String input, MC_EventInfo event) {
		if (event.isCancelled) {
			return; // cancelled by other plugin
		}
		if (input.startsWith("/")) {
			return; // filter out commands
		}
		getChatManager().sendChat(player, input);
		event.isCancelled = true; // cancel event so other plugins and the server dont process it.
	}
}
